/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mukesh;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * Toast工具类
 *
 * @since 2021-04-19
 */
public class Toast {
    private static final int LENGTH_SHORT = 2000;
    private static final int LENGTH_LONG = 4000;
    private static final int VP_FOURTEEN = 14;
    private static final int VP_TWENTY = 20;
    private static final int VP_TWENTY_TWO = 22;

    private Toast() {
    }

    /**
     * Toast位置标签
     *
     * @since 2021-04-17
     */
    public enum ToastLayout {
        /**
         * 默认
         */
        DEFAULT,
        /**
         * 中间
         */
        CENTER,
        /**
         * 顶部
         */
        TOP,
        /**
         * 底部
         */
        BOTTOM,
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     */
    public static void show(Context context, String content) {
        createTost(context, content, LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     * @param duration
     */
    public static void show(Context context, String content, int duration) {
        createTost(context, content, duration, ToastLayout.DEFAULT);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     * @param layout
     */
    public static void show(Context context, String content, ToastLayout layout) {
        createTost(context, content, LENGTH_SHORT, layout);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     * @param duration
     * @param layout
     */
    public static void show(Context context, String content, int duration, ToastLayout layout) {
        createTost(context, content, duration, layout);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     */
    public static void show(Context context, int content) {
        createTost(context, getString(context, content), LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     * @param duration
     */
    public static void show(Context context, int content, int duration) {
        createTost(context, getString(context, content), duration, ToastLayout.DEFAULT);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     * @param layout
     */
    public static void show(Context context, int content, ToastLayout layout) {
        createTost(context, getString(context, content), LENGTH_SHORT, layout);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     * @param duration
     * @param layout
     */
    public static void show(Context context, int content, int duration, ToastLayout layout) {
        createTost(context, getString(context, content), duration, layout);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     */
    public static void showShort(Context context, int content) {
        createTost(context, getString(context, content), LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     */
    public static void showShort(Context context, String content) {
        createTost(context, content, LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     */
    public static void showLong(Context context, String content) {
        createTost(context, content, LENGTH_LONG, ToastLayout.DEFAULT);
    }

    /**
     * 显示弹框
     *
     * @param context
     * @param content
     */
    public static void showLong(Context context, int content) {
        createTost(context, getString(context, content), LENGTH_LONG, ToastLayout.DEFAULT);
    }

    private static void createTost(Context context, String content, int duration, ToastLayout layout) {
        DirectionalLayout.LayoutConfig textConfig = new DirectionalLayout.LayoutConfig(
                DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        Text text = new Text(context);
        text.setText(content);
        text.setTextColor(new Color(Color.getIntColor("#000000")));
        text.setPadding(vp2px(context, VP_FOURTEEN), vp2px(context, VP_FOURTEEN),
                vp2px(context, VP_FOURTEEN), vp2px(context, VP_FOURTEEN));
        text.setTextSize(vp2px(context, VP_FOURTEEN));
        text.setBackground(buildDrawableByColorRadius(Color.getIntColor("#f2f2f2"), vp2px(context, VP_TWENTY)));
        text.setLayoutConfig(textConfig);
        text.setMarginBottom(vp2px(context, VP_TWENTY_TWO));
        DirectionalLayout toastLayout = new DirectionalLayout(context);
        toastLayout.addComponent(text);
        int mLayout = LayoutAlignment.BOTTOM;
        switch (layout) {
            case TOP:
                mLayout = LayoutAlignment.TOP;
                break;
            case BOTTOM:
                mLayout = LayoutAlignment.BOTTOM;
                break;
            case CENTER:
                mLayout = LayoutAlignment.CENTER;
                break;
            default:
                break;
        }
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setComponent(toastLayout);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(mLayout);
        toastDialog.setTransparent(true);
        toastDialog.setDuration(duration);
        toastDialog.show();
    }

    private static ohos.agp.components.element.Element buildDrawableByColorRadius(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(0);
        drawable.setRgbColor(RgbColor.fromArgbInt(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    private static String getString(Context content, int resId) {
        try {
            return content.getResourceManager().getElement(resId).getString();
        } catch (IOException | WrongTypeException | NotExistException exc) {
            LogUtil.e("resId not exist..");
        }
        return "";
    }

    private static int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (attributes.densityPixels * vp);
    }
}