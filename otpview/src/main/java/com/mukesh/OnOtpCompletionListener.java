/*
 * Copyright 2018 Mukesh Solanki
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mukesh;

/**
 * 输入的监听抽象类
 * 没定义接口的原因是可以在抽象类里面定义空实现的方法,可以让用户根据需求选择性的复写某些方法
 *
 * @since 2021-03-25
 */
public abstract class OnOtpCompletionListener {
    /**
     * 输入完成的抽象方法
     *
     * @param content 输入内容
     */
    public abstract void onOtpCompleted(String content);

    /**
     * 输入的内容
     * 定义一个空实现方法,让用户选择性的复写该方法,需要就复写,不需要就不用重写
     *
     * @param text 更新的内容
     */
    public void onInputChanged(String text) {
    }
}
