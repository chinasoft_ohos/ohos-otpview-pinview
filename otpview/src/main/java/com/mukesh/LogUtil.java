/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain the copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.mukesh;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Log工具类
 *
 * @since 2021-03-25
 */
public class LogUtil {
    /**
     * Log TAG
     */
    public static final String TAG = "otpview_pinview";
    private static final boolean isDebug = false;
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, TAG);

    private LogUtil() {
    }

    /**
     * debug
     *
     * @param msg 日志内容
     */
    public static void d(String msg) {
        if (!isDebug) {
            HiLog.debug(LABEL, msg);
        }
    }

    /**
     * debug
     *
     * @param msg 日志内容
     */
    public static void e(String msg) {
        if (!isDebug) {
            HiLog.error(LABEL, msg);
        }
    }

    /**
     * debug
     *
     * @param msg 日志内容
     */
    public static void w(String msg) {
        if (!isDebug) {
            HiLog.warn(LABEL, msg);
        }
    }

    /**
     * debug
     *
     * @param msg 日志内容
     */
    public static void i(String msg) {
        if (!isDebug) {
            HiLog.info(LABEL, msg);
        }
    }

    /**
     * debug
     *
     * @param msg 日志内容
     */
    public static void f(String msg) {
        if (!isDebug) {
            HiLog.fatal(LABEL, msg);
        }
    }
}
