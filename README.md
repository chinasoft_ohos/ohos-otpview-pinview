# ohos-otpview-pinview

#### 项目介绍
- 项目名称：ohos-otpview-pinview
- 所属系列：openharmony 第三方组件适配移植
- 功能：用于在身份验证时输入四位数代码
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 2.1.2

#### 效果演示
![](/screenshots/a1.gif)

#### 安装教程

在moudle级别下的build.gradle文件中添加依赖 
```
// 添加maven仓库 
repositories { 
    maven { 
        url 'https://s01.oss.sonatype.org/content/repositories/releases/' 
    }
}

// 添加依赖库
dependencies {
    implementation 'com.gitee.chinasoft_ohos:ohos-otpview-pinview:1.0.1'   
}
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
**相关属性:**

* **OtpItemCount** 密码框长度
* **OptcontentShowMode** 密码显示模式
* **OtpItemSpacing** 密码框之间的间距
* **OtpLineWidth** 密码框线条宽度
* **OtpLineColor** 密码框线条颜色
* **OtpViewType** 密码框显示类型
* **OtpCursorVisible** 是否显示光标
* **OtpCursorColor** 光标颜色
* **OtpCursorWidth** 光标宽度
* **OtpItemBackground** 密码框条目的背景
* **OtpHideLineWhenFilled** 切换边界
* **OtpRtlTextDirection** 切换输入方向
* **OtpState_filled** 切换数据输入后填充字段(文件的样式可以用itemBackground指定的绘制来设置

``` xml
   <com.mukesh.OtpView
        ohos:id="$+id:otp_view"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:text_color="$color:wihte"
        ohos:multiple_lines="false"
        ohos:text_size="30fp"
        ohos:top_margin="72vp"
        app:OtpLineColor="$color:colorAccent"
        app:OtpLineWidth="2vp"
        app:OtpItemCount="6"
        app:OptcontentShowMode="2"
        app:corner_size="0vp"
        app:OtpCursorColor="$color:colorPrimary"
        app:OtpCursorWidth="3vp"
        app:inputBoxSquare="true"
        app:inputBoxStyle="2"
        app:otpViewType="1"
        app:OtpItemSpacing="5vp"
        app:underlineFocusColor="$color:colorAccent"
        app:underlineNormalColor="$color:colorPrimary"/>
```

使用OnOtpCompletionListener来监听选中的变换

```java
 OtpView otpView = (OtpView) findComponentById(ResourceTable.Id_otp_view);
        otpView.setListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String content) {
                Toast.show(getContext(), content);
            }
        });
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.1

#### 版权和许可信息
    MIT License

    Copyright (c) 2018 Mukesh Solanki

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
