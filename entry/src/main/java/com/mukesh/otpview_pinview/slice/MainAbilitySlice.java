/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mukesh.otpview_pinview.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.miscservices.pasteboard.PasteData;
import ohos.miscservices.pasteboard.SystemPasteboard;
import ohos.vibrator.agent.VibratorAgent;
import ohos.vibrator.bean.VibrationPattern;

import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.mukesh.Toast;
import com.mukesh.otpview_pinview.ResourceTable;

import java.util.List;

/**
 * 入口
 *
 * @since 2021-04-17
 */
public class MainAbilitySlice extends AbilitySlice {
    private OtpView otpView;
    private PopupDialog dialog;
    private SystemPasteboard pasteboard;
    private VibratorAgent mVibratorAgent;
    private List<Integer> mVibratorList;
    private Integer mVibratorId;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        if (mVibratorAgent == null) {
            mVibratorAgent = new VibratorAgent();
        }
        mVibratorList = mVibratorAgent.getVibratorIdList();
        if (!mVibratorList.isEmpty()) {
            mVibratorId = mVibratorList.get(0);
        }

        otpView = (OtpView) findComponentById(ResourceTable.Id_otp_view);
        otpView.setListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String content) {
                Toast.show(getContext(), "OnOtpCompletionListener called");
            }
        });
        findComponentById(ResourceTable.Id_btnChangeMode).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (otpView.getContentShowMode() == OtpView.CONTENT_SHOW_MODE_PASSWORD) {
                    otpView.setContentShowMode(OtpView.CONTENT_SHOW_MODE_TEXT);
                } else {
                    otpView.setContentShowMode(OtpView.CONTENT_SHOW_MODE_PASSWORD);
                }
            }
        });
        findComponentById(ResourceTable.Id_validate_button).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                OtpView otpView = (OtpView) findComponentById(ResourceTable.Id_otp_view);
                String str = otpView.getText();
                Toast.show(getContext(), str);

            }
        });
//        -----------------
        pasteboard = SystemPasteboard.getSystemPasteboard(getContext());
        otpView.setLongClickedListener(new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
//                showPastePopupDialog(); // 简单的粘贴功能
                // 震动反馈
                mVibratorAgent.startOnce(mVibratorId,
                    VibrationPattern.VIBRATOR_TYPE_CONTROL_TEXT_EDIT);
            }
        });
    }

    private void showPastePopupDialog() {
        if (dialog == null) {
            Component dialogView = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_popup_dialog_paste, null, true);
            dialog = new PopupDialog(getContext(), otpView);
            dialog.setCustomComponent(dialogView);
            dialog.setCornerRadius(20);
            dialog.setMode(LayoutAlignment.TOP | LayoutAlignment.LEFT);
            dialog.show();
            dialogView.findComponentById(ResourceTable.Id_btn).setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    pasteData();
                    dialog.destroy();
                    dialog = null;
                }
            });

        } else {
//            dialog.show();
        }
    }

    private void pasteData() {
        PasteData pasteData = pasteboard.getPasteData();
        if (pasteData == null) {
            return;
        }
        PasteData.DataProperty dataProperty = pasteData.getProperty();
        boolean hasHtml = dataProperty.hasMimeType(PasteData.MIMETYPE_TEXT_HTML);
        boolean hasText = dataProperty.hasMimeType(PasteData.MIMETYPE_TEXT_PLAIN);
        if (hasHtml || hasText) {
            for (int i = 0; i < pasteData.getRecordCount(); i++) {
                PasteData.Record record = pasteData.getRecordAt(i);
                String mimeType = record.getMimeType();
                if (mimeType.equals(PasteData.MIMETYPE_TEXT_HTML)) {
                    otpView.setText(record.getHtmlText());
                    break;
                } else if (mimeType.equals(PasteData.MIMETYPE_TEXT_PLAIN)) {
                    String text = otpView.getText();
                    if (text != null) {
                        otpView.setText(text + record.getPlainText().toString());
                    } else {
                        otpView.setText(record.getPlainText().toString());
                    }
                    break;
                } else {
                    // skip records of other Mime type
                }
            }
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
